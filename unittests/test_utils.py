#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2020 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import os
import unittest

from scifolder.utils import get_xls_header


class XLSTest(unittest.TestCase):
    def test_read(self):
        filename = os.path.join(os.path.dirname(__file__),
                                "data/README.xlsx")
        assert os.path.exists(filename)

        header = get_xls_header(filename)
        assert header is not None
        assert isinstance(header, dict)

        # responsible
        assert header['responsible'] == ["Ana Lytic"]

        # description
        assert len(header['description']) == 1
        assert isinstance(header['description'][0], str)
        assert len(header['description'][0]) > 20
        assert "exciting" in header['description'][0]

        # sources
        assert isinstance(header['sources'], list)

        for el in header['sources']:
            assert isinstance(el, dict)
            assert "TestProject" in el["file"]
            assert "example" in el["description"]

        # scripts
        assert isinstance(header['scripts'], list)

        for el in header['scripts']:
            assert isinstance(el, dict)
            assert "scripts" == el["file"]
            assert "all the files" in el["description"]

        # results
        assert isinstance(header['results'], list)

        for el in header['results']:
            assert isinstance(el, dict)
            assert "result.pdf" == el["file"]
            assert "plot" in el["description"]
